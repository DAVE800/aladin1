(function () {
  "use strict";

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkaladin"] = self["webpackChunkaladin"] || []).push([["src_app_cart_cart_module_ts"], {
    /***/
    63951: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "CartRoutingModule": function CartRoutingModule() {
          return (
            /* binding */
            _CartRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _panier_panier_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./panier/panier.component */
      86363);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      71258);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      2316);

      var routes = [{
        path: '',
        component: _panier_panier_component__WEBPACK_IMPORTED_MODULE_0__.PanierComponent
      }];

      var _CartRoutingModule = function _CartRoutingModule() {
        _classCallCheck(this, _CartRoutingModule);
      };

      _CartRoutingModule.ɵfac = function CartRoutingModule_Factory(t) {
        return new (t || _CartRoutingModule)();
      };

      _CartRoutingModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: _CartRoutingModule
      });
      _CartRoutingModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](_CartRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule]
        });
      })();
      /***/

    },

    /***/
    12943: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "CartModule": function CartModule() {
          return (
            /* binding */
            _CartModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../shared */
      51679);
      /* harmony import */


      var _cart_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./cart-routing.module */
      63951);
      /* harmony import */


      var _panier_panier_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./panier/panier.component */
      86363);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      2316);

      var _CartModule = function _CartModule() {
        _classCallCheck(this, _CartModule);
      };

      _CartModule.ɵfac = function CartModule_Factory(t) {
        return new (t || _CartModule)();
      };

      _CartModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({
        type: _CartModule
      });
      _CartModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({
        imports: [[_cart_routing_module__WEBPACK_IMPORTED_MODULE_1__.CartRoutingModule, _shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](_CartModule, {
          declarations: [_panier_panier_component__WEBPACK_IMPORTED_MODULE_2__.PanierComponent],
          imports: [_cart_routing_module__WEBPACK_IMPORTED_MODULE_1__.CartRoutingModule, _shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule]
        });
      })();
      /***/

    },

    /***/
    86363: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PanierComponent": function PanierComponent() {
          return (
            /* binding */
            _PanierComponent
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      2316);
      /* harmony import */


      var _shared_layout_header_header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../../shared/layout/header/header.component */
      34162);
      /* harmony import */


      var _shared_cartitems_cartitems_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../shared/cartitems/cartitems.component */
      61756);
      /* harmony import */


      var _shared_layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../shared/layout/footer/footer.component */
      71070);

      var _PanierComponent = /*#__PURE__*/function () {
        function _PanierComponent() {
          _classCallCheck(this, _PanierComponent);
        }

        _createClass(_PanierComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return _PanierComponent;
      }();

      _PanierComponent.ɵfac = function PanierComponent_Factory(t) {
        return new (t || _PanierComponent)();
      };

      _PanierComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({
        type: _PanierComponent,
        selectors: [["app-panier"]],
        decls: 7,
        vars: 0,
        template: function PanierComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "app-header");

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](2, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](4, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](5, "app-cartitems");

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "app-footer");
          }
        },
        directives: [_shared_layout_header_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent, _shared_cartitems_cartitems_component__WEBPACK_IMPORTED_MODULE_1__.CartitemsComponent, _shared_layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_2__.FooterComponent],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYW5pZXIuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_cart_cart_module_ts-es5.js.map