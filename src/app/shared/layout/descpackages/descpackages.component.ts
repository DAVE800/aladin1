
import { Component, OnInit,Input,Output,EventEmitter ,OnChanges} from '@angular/core';
import { AladinService } from 'src/app/core';
declare var require: any
var $ = require("jquery");
var myalert=require('sweetalert2');
@Component({
  selector: 'app-descpackages',
  templateUrl: './descpackages.component.html',
  styleUrls: ['./descpackages.component.scss']
})
export class DescpackagesComponent implements OnInit ,OnChanges{
@Input() data:any;
@Output() changeComponent=new EventEmitter<boolean>();
@Output() showaladin=new EventEmitter<any>()
quantite=100;
chantp=true
cpt=1
head=true
size:any
Changevalue=true;
  constructor(private aladin:AladinService) { 

  }
  ngOnInit(): void {
    
   console.log(this.data)
    //this.data.show=true;
  }
ngOnChanges() : void{
 
}


go(event:any){
 if(this.cpt>0&&this.size){
  Object.assign(this.data,{
    aladin:true,
    category:"packs",
    qtys:this.cpt,
    t: this.cpt * this.data.price,
    size:this.size,
    show:false
  })
  this.aladin.ShowEditor.next(this.data)
  console.log(this.data)
 }else{
  myalert.fire({
    title:'<strong>Erreur</strong>',
    icon:'error',
    html:
      
      '<p style="color:green">definissez les caracterique de votre design svp !!!</p> ' 
      ,
    showCloseButton: true,
    focusConfirm: false,
  
  })
 }
    
}


  reload(value:boolean){  
    this.changeComponent.emit(value);
    this.data.show=false;


  }
plusqty(){
  this.cpt++
this.quantite=this.quantite + 100
console.log(this.cpt * this.data.price)


}
minusqty(){
  
 if(this.cpt>1){
  this.cpt--
  this.quantite=this.quantite - 100
console.log(this.cpt * this.data.price, this.cpt)
 } else{
   this.quantite=this.quantite
 }



}

onchange(event:any){
if(event.target.value==this.data.size.t1){
  this.size=this.data.size.t1
 
  }
  if(event.target.value==this.data.size.t2){
    this.size=this.data.size.t2
    
  }
  if(event.target.value==this.data.size.t3){
      this.size=this.data.size.t3
      
  }
  if(event.target.value==this.data.size.t4){
       this.size=this.data.size.t4
      
  }
  if(event.target.value==this.data.size.t5){
   this.size=this.data.size.t5
   
  }
  
  if(event.target.value==this.data.size.t6){
   this.size=this.data.size.t6
    
  }
  if(event.target.value==this.data.size.t7){
    this.size=this.data.size.t7
    
  }
  console.log(this.size)
}
ChangeComponent(value:boolean){
  this.changeComponent.emit(value)
  console.log(value)

}
}

