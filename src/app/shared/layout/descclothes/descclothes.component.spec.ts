import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescclothesComponent } from './descclothes.component';

describe('DescclothesComponent', () => {
  let component: DescclothesComponent;
  let fixture: ComponentFixture<DescclothesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescclothesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescclothesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
