import { Component, OnInit,Input,OnChanges ,EventEmitter,Output} from '@angular/core';
import { AladinService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
@Component({
  selector: 'app-descclothes',
  templateUrl: './descclothes.component.html',
  styleUrls: ['./descclothes.component.scss']
})

export class DescclothesComponent implements OnInit,OnChanges {

@Input() data:any;
@Output() changeComponent=new EventEmitter<boolean>();
Changevalue=true
chantp=true
url="/editor/cloth/"
QtM=1;
 QtS=0;
 QtL=0;
 QtXL=0;
 QtXXL=0;
 QtXXXL=0;
 QTY:any
 Qtotal:any=0
 typimpr=["Flexographie","Seriegraphie","Sublimation","Borderie","Transfert"]
 impr:any
 head=true
ngOnChanges():void{
  console.log(this.data)

}

  constructor(private aladin:AladinService) { }

  ngOnInit(): void {
    //this.data.show=false;
    
    console.log(this.data)

  }
  onchange(event:any){
    if(event.target.value=="Flexographie"){
      this.impr="Flexographie"
    }
    if(event.target.value=="Seriegraphie"){
      this.impr="Seriegraphie"
    }
    if(event.target.value=="Sublimation"){
      this.impr="Sublimation"
    }
    if(event.target.value=="Borderie"){
      this.impr="Borderie"
    }
    if(event.target.value=="Transfert"){
      this.impr="Transfert"
    }
    console.log(this.impr)
  }
  go(event:any){
    this.QTY={
      ql:this.QtL,
      qs:this.QtS,
      qxl:this.QtXL,
      qxxl:this.QtXXL,
      qxxxl:this.QtXXXL,
      qm:this.QtM

    }
     this.Qtotal=this.QtM + this.QtL +this.QtS + this.QtXL + this.QtXXL+this.QtXXXL
   if(this.Qtotal>0 && this.impr){
    Object.assign(this.data,{
      aladin:true,
      //editor:true,
      category:"clothes",
      type:this.impr,
      size_type:this.QTY,
      t: this.data.price * this.Qtotal,
      qtys:this.Qtotal

    })
    this.aladin.ShowEditor.next(this.data)
    console.log(this.data)
   }else{
    myalert.fire({
      title:'<strong>Erreur</strong>',
      icon:'error',
      html:
        
        '<p style="color:green">definissez les caracterique de votre design svp !!!</p> ' 
        ,
      showCloseButton: true,
      focusConfirm: false,
    
    })
  }

  }

//reload(value:boolean){
 //this.data.show=!this.Changevalue
  //this.changeComponent.emit(value)

//}
  //quantite de la taille M
  QtplusM(event:any){
    this.QtM =this.QtM +1;
   console.log(this.QtM)
    
  }
  QtminusM(event:any){
    if(this.QtM>0){
   this.QtM =this.QtM-1
  
    }else{
     this.QtM=+this.QtM
    
    }

  }
  //quantité de la taille S
  QtplusS(event:any){
   this.QtS =+this.QtS+1
   console.log(event)
 }
 QtminusS(event:any){
   if(this.QtS>0){
  this.QtS =+this.QtS-1
   }else{
    this.QtS=+this.QtS
   }
 }
  //quantité de la taille L
  QtplusL(event:any){
   this.QtL =+this.QtL+1
   console.log(event)
 }
 QtminusL(event:any){
   if(this.QtL>0){
  this.QtL =+this.QtL-1
   }else{
    this.QtL=+this.QtL
   }
 }
  //quantité de la taille XL
  QtplusXL(event:any){
   this.QtXL =+this.QtXL+1
   console.log(event)
 }
 QtminusXL(event:any){
   if(this.QtXL>0){
  this.QtXL =+this.QtXL-1
   }else{
    this.QtXL=+this.QtXL
   }
 }
  //quantité de la taille XXL
  QtplusXXL(event:any){
    this.QtXXL =+this.QtXXL+1
    console.log(event)
    console.log(this.QtXXL)
  }
  QtminusXXL(event:any){
    if(this.QtXXL>0){
   this.QtXXL =+this.QtXXL-1
    }else{
     this.QtXXL=+this.QtXXL
    }
    console.log(this.QtXXL)
  }

  //quantité de la taille XXXL
  QtplusXXXL(event:any){
    this.QtXXXL =+this.QtXXXL+1
    console.log(event)
    console.log(this.QtXXXL)
  }
  QtminusXXXL(event:any){
    if(this.QtXXXL>0){
   this.QtXXXL =+this.QtXXXL-1
    }else{
     this.QtXXXL=+this.QtXXL
    }
    console.log(this.QtXXXL)
  }
  ChangeComponent(value:boolean){
    this.changeComponent.emit(value)
    console.log(value)
  
  }
}
