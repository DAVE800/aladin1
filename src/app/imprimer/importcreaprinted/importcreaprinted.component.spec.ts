import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportcreaprintedComponent } from './importcreaprinted.component';

describe('ImportcreaprintedComponent', () => {
  let component: ImportcreaprintedComponent;
  let fixture: ComponentFixture<ImportcreaprintedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportcreaprintedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportcreaprintedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
